import React, { useRef } from "react";
import { Editor } from "@tinymce/tinymce-react";
import {
  Wrapper as ExtensionWrapper,
  FieldExtensionType,
  FieldExtensionFeature,
  useUiExtension,
} from "@graphcms/uix-react-sdk";

const declaration = {
  extensionType: "field",
  fieldType: FieldExtensionType.STRING,
  name: "TinyMCE Editor",
  description: "Edit HTML with tinyMCE",
  features: [FieldExtensionFeature.FieldRenderer],
};

export default function App() {
  const uid = new URLSearchParams(document.location.search).get("extensionUid");
  console.log({ uid });
  return (
    <ExtensionWrapper uid={uid} declaration={declaration}>
      <WrappedEditor />
    </ExtensionWrapper>
  );
}

export function WrappedEditor() {
  const { expandField, isExpanded, value, onChange, openAssetPicker } =
    useUiExtension();
  const editorRef = useRef(null);
  const expandRef = useRef(false);
  const initialValue = useRef(value);
  expandRef.current = isExpanded;
  return (
    <div style={{ paddingLeft: '10px', paddingRight: '10px', paddingBottom: '100px', minHeight: '400px'}}>
      <Editor
        tinymceScriptSrc={process.env.PUBLIC_URL + "/tinymce/tinymce.min.js"}
        onInit={(evt, editor) => {
          editorRef.current = editor;
        }}
        initialValue={initialValue.current}
        onEditorChange={(value) => onChange(value)}
        init={{
          height: "400px",
          menubar: false,
          setup: function (editor) {
            editor.ui.registry.addButton("uixfullscreen", {
              tooltip: "Toggle fullscreen",
              icon: "new-tab",
              onAction: function (api) {
                expandField(!expandRef.current);
              },
            });
          },
          plugins: [
            "print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons",
          ],
          toolbar:
            "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | charmap emoticons | fullscreen  preview | insertfile image media link anchor codesample | table",
          content_style:
            "body { font-family:Sen,Arial,sans-serif; font-size:14px }",
          quickbars_insert_toolbar: false,
          toolbar_mode: "sliding",
          file_picker_callback: async function (callback, value, meta) {
            let asset = await openAssetPicker();
            callback(asset.url);
          },
        }}
      />
    </div>
  );
}
